#!/bin/bash

#RUN FLAGS

if [ "$1" == "--inactive" ] ; then
	echo "Making the list INCLUDING satellites marked as inactive"
	inactive=true;
elif [ "$1" == "--help" ] ; then
	echo "This is a script designed to scrape the webpage of DK3WN"
	echo "The page can be found on http://www.dk3wn.info/p/?page_id=29535"
	echo ".."
	echo "IF YOU WANT TO MAKE THE LIST INCLUDING SATELLITES MARKED AS INACTIVE"
	echo "USE --inactive"
	echo "PROVIDED WITHOUT ANY WARRANTY!"
	echo "USE ON YOUR OWN RISK"
	echo ""
	echo ""
	echo "I want to extend my gratitude towards DK3WN for providing the data."
	echo "You are doing a great job"
	echo "Thanks!"
	echo ""
	echo ""
	echo "LA6XTA - Per Kristian Fjellby"
	exit 0
else
	echo "Making the list EXCLUDING satellites marked as inactive"
	inactive=false;
fi

rawstorage="parsed.txt"
predictdb="predict.tmp"


#Cleanup
echo 'removing previous tle files'
> $predictdb
sleep 1
echo 'removed'
echo '.'
echo ' .'
echo '  .'

#Run the ruby HTML table parser and wait for it to finish.
./parser.rb | sed -e '2,11d' > $rawstorage 
sleep 2


echo "The data on the webpage was last updated: " `awk 'NR == 1' $rawstorage`


echo 'starting while loop'

#needs to start from 1 because of date string on first line
i=1
count=`(wc -l < $rawstorage)`

while [ "$i" -le $[$count-10] ]
do
#echo $i
name=`awk -v var="$[i+1]" 'NR == var' $rawstorage`
state=`awk -v var="$[i+2]" 'NR == var' $rawstorage`
noradid=`awk -v var="$[i+3]" 'NR == var' $rawstorage`
beacon=`awk -v var="$[i+6]" 'NR == var' $rawstorage`
uplink=`awk -v var="$[i+4]" 'NR == var' $rawstorage`
downlink=`awk -v var="$[i+5]" 'NR == var' $rawstorage`
mode=`awk -v var="$[i+7]" 'NR == var' $rawstorage`
callsign=`awk -v var="$[i+8]" 'NR == var' $rawstorage`

#Check INACTIVE flag
if [ $state = "INACTIVE" ] && [ $inactive = "false" ]; then
echo "skipped $name"
i=$[i+10]
continue
fi

#Printout
echo $name >> $predictdb
echo $noradid >> $predictdb
echo 'No alat, alon' >> $predictdb
#Beacon
if [ "${#beacon}" -gt "1" ]
then
	#Only downling, so set uplink to 0
	split1=`echo $beacon | cut -d "/" -f 1`
	split2=`echo $beacon | cut -d "/" -f 2`
	split1split1=`echo $split1 | cut -d "." -f 1`
	greptest=`echo $beacon | grep "/"`
	if [ "${#greptest}" -gt "0" ]
	then	
		modesplit1=`echo $mode | cut -d "/" -f 1`
		modesplit2=`echo $mode | cut -d "/" -f 2`
		#first
		echo 'Beacon' >> $predictdb
		echo '0.0, 0.0' >> $predictdb
		echo "$split1, $split1" >> $predictdb 
		#second
		echo 'Beacon' >> $predictdb
		echo '0.0, 0.0' >> $predictdb
		echo "$split1split1.$split2, $split1split1.$split2" >> $predictdb
	else
		echo 'Beacon' >> $predictdb
		echo '0.0, 0.0' >> $predictdb
		echo "$split1, $split1" >> $predictdb
	fi
fi	
if [ "${#uplink}" -gt "1" ] || [ "${#downlink}" -gt "1" ]
then
	#145.20/144.49
	#432.125-175
	udrift1=`echo $uplink | cut -d "-" -f 1`
	udrift1first=`echo $udrift1 | cut -d "." -f 1`	#432
	udrift2=`echo $uplink | cut -d "-" -f 2`	#174
	usplit1=`echo $uplink | cut -d "/" -f 1`	#145.20
	usplit2=`echo $uplink | cut -d "/" -f 2`	#144.49
	udrift1first=`echo $uplink | cut -d "." -f 1`
	
	ddrift1=`echo $downlink | cut -d "-" -f 1`	
	ddrift1first=`echo $ddrift | cut -d "." -f 1`		
	ddrift2=`echo $downlink | cut -d "-" -f 2`	
	dsplit1=`echo $downlink | cut -d "/" -f 1`	
	dsplit2=`echo $downlink | cut -d "/" -f 2`	
	ddrift1first=`echo $downlink | cut -d "." -f 1`


	grepup=`echo $uplink | grep "/"`
	grepdown=`echo $downlink | grep "/"`
	if [[ "${#grepup}" -gt "0" ]] && [[ "${#grepdown}" -eq "0" ]]
	then
		#Uplink
		echo "Voice (Mode: $mode)" >> $predictdb
		echo "$usplit1, $usplit1" >> $predictdb
		if [ "${#downlink}" -gt "1" ]; then
			echo $ddrift1, $ddrift1first.$ddrift2 >> $predictdb
		else
			echo '0, 0' >> $predictdb
		fi
		echo "Voice 2 (Mode: $mode)" >> $predictdb
		echo "$usplit2, $usplit2" >> $predictdb
		if [ "${#downlink}" -gt "1" ]; then
			echo "$ddrift1, $ddrift1first.$ddrift2" >> $predictdb
		else
			echo "0, 0" >> $predictdb
		fi	
	fi
	if [[ "${#grepup}" -eq "0" ]] && [[ "${#grepdown}" -gt "0" ]]
	then
		echo "Voice (Mode: $mode)" >> $predictdb
		if [ "${#uplink}" -gt "1" ]; then
			echo "$udrift1, $udrift1first.$udrift2" >> $predictdb
		else
			echo "0, 0" >> $predictdb
		fi
		echo "$dsplit1, $dspli1" >> $predictdb
		echo "Voice 2 (Mode: $mode )" >> $predictdb
		if [ "${#uplink}" -gt "1" ]; then
			echo "$udrift1, $udrift1first.$udrift2" >> $predictdb
		else
			echo "0, 0" >> $predictdb
			echo "$dspli2, $dsplit2" >> $predictdb
		fi
	fi
	#if We got no "/" in the table, process as "normal"
	if [[ "${#grepup}" -eq "0" ]] && [[ "${#grepdown}" -eq "0" ]]
	then
		grepsplitu=`echo $uplink | grep "-"`
		grepsplitd=`echo $downlink | grep "-"`
		echo "Voice (Mode: $mode)" >> $predictdb
		if [[ "${#uplink}" -gt "1" ]] && [[ "${#grepsplitu}" -gt "1" ]]
		then
			echo "$udrift1, $udrift1first.$udrift2" >> $predictdb
		elif [[ "${#uplink}" -gt "1" ]] && [[ "${#grepsplitu}" -eq "0" ]]
		then
			echo "$uplink, $uplink" >> $predictdb
		else
			echo "0, 0" >> $predictdb
		fi	
		if [[ "${#downlink}" -gt "1" ]] && [[ "${#grepsplitd}" -gt "1" ]]
		then
			echo "$ddrift1, $ddrift1first.$ddrift2" >> $predictdb	
		elif [[ "${#downlink}" -gt "1" ]] && [[ "${#grepsplitd}" -eq "0" ]]
		then	
			echo "$downlink, $downlink" >> $predictdb
		else
			echo "0, 0" >> $predictdb
		fi	
	fi
fi

echo 'No weekly schedule' >> $predictdb
echo 'No orbital schedule' >> $predictdb
echo 'end' >> $predictdb
i=$[i+10]
done
echo 'finished'
